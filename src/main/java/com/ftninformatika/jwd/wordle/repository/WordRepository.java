package com.ftninformatika.jwd.wordle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.wordle.model.Word;

@Repository
public interface WordRepository extends JpaRepository<Word, Long> {

	Word findOneById(Long id);

	Word findByWordIgnoreCaseContains(String word);
}
