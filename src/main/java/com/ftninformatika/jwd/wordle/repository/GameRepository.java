package com.ftninformatika.jwd.wordle.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ftninformatika.jwd.wordle.model.Game;

public interface GameRepository extends JpaRepository<Game, Long> {

	Game findOneById(Long id);

}
