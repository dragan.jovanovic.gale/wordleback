package com.ftninformatika.jwd.wordle.service;

import com.ftninformatika.jwd.wordle.model.Game;

public interface GameService {

	Game save(Game game);

	Game findOneById(Long id);

	Game update(Game game);

	Game checkLettersAndGameStatus(String guess, String word, Integer numberOfTries);

}
