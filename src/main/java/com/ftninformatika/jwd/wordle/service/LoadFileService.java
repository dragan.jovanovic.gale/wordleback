package com.ftninformatika.jwd.wordle.service;

import java.util.List;

public interface LoadFileService {

	void save(List<String> words);

}
