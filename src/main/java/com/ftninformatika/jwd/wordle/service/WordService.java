package com.ftninformatika.jwd.wordle.service;

import com.ftninformatika.jwd.wordle.model.Word;

public interface WordService {

	Word getOne();

	Word findOneById(Long id);

	Word findByWordIgnoreCaseContains(String word);
}
