package com.ftninformatika.jwd.wordle.dto;

public class GameDto {

	private Long id;

	private int numberOfTries;

	private String word;

	private Long wordId;

	private String guessWord;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumberOfTries() {
		return numberOfTries;
	}

	public void setNumberOfTries(int numberOfTries) {
		this.numberOfTries = numberOfTries;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public Long getWordId() {
		return wordId;
	}

	public void setWordId(Long wordId) {
		this.wordId = wordId;
	}

	public String getGuessWord() {
		return guessWord;
	}

	public void setGuessWord(String guessWord) {
		this.guessWord = guessWord;
	}

}
