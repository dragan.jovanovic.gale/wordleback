package com.ftninformatika.jwd.wordle.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class WordDTO {

	    @NotBlank
	    @Size(min=5, max=5)
	    private String word;
	    
	    private Long id;
	    
	    

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getWord() {
			return word;
		}

		public void setWord(String word) {
			this.word = word;
		}
	    
	    

}
