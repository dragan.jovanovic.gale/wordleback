package com.ftninformatika.jwd.wordle.dto;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class GuessResponseSubmitDto {

	@Positive
	private Long id;

	@Size(max = 5, min = 5)
	private String word;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

}
