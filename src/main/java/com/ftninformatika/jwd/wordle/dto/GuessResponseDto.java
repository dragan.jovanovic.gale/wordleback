package com.ftninformatika.jwd.wordle.dto;

import java.util.HashMap;
import java.util.Map;

import com.ftninformatika.jwd.wordle.enumer.GameStatus;
import com.ftninformatika.jwd.wordle.enumer.Letter;

public class GuessResponseDto {

	private String word;

	private GameStatus status;

	private Map<Integer, Letter> characters = new HashMap<>();

	private int currentTry;

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public GameStatus getStatus() {
		return status;
	}

	public void setStatus(GameStatus status) {
		this.status = status;
	}

	public Map<Integer, Letter> getCharacters() {
		return characters;
	}

	public void setCharacters(Map<Integer, Letter> characters) {
		this.characters = characters;
	}

	public int getCurrentTry() {
		return currentTry;
	}

	public void setCurrentTry(int currentTry) {
		this.currentTry = currentTry;
	}

}
