package com.ftninformatika.jwd.wordle.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.wordle.enumer.GameStatus;
import com.ftninformatika.jwd.wordle.enumer.Letter;
import com.ftninformatika.jwd.wordle.model.Game;
import com.ftninformatika.jwd.wordle.repository.GameRepository;
import com.ftninformatika.jwd.wordle.service.GameService;

@Service
public class JpaGameService implements GameService {

	@Autowired
	private GameRepository gameRepo;

	@Override
	public Game save(Game game) {
		return gameRepo.save(game);
	}

	@Override
	public Game findOneById(Long id) {
		return gameRepo.findOneById(id);
	}

	@Override
	public Game update(Game game) {
		return gameRepo.save(game);
	}

	@Override
	public Game checkLettersAndGameStatus(String guess, String word, Integer numberOfTries) {
		Map<Integer, Letter> characters = new HashMap<>();
		Map<Character, Integer> correctWord = new LinkedHashMap<>();

		System.out.println(guess);
		System.out.println(word);

		for (int i = 0; i < guess.length(); i++) {
			if (correctWord.get(word.charAt(i)) == null) {

				correctWord.put(word.charAt(i), 1);
			} else {
				correctWord.put(word.charAt(i), correctWord.get(word.charAt(i)) + 1);
			}
		}

		Game game = new Game();

		if (word.equalsIgnoreCase(guess)) {
			for (int i = 0; i < guess.length(); i++) {
				characters.put(i, Letter.CORRECT);
				correctWord.put(guess.charAt(i), correctWord.get(guess.charAt(i)) - 1);
			}
			game.setNumberOfTries(numberOfTries++);
			game.setGameStatus(GameStatus.WIN);
			game.setCharacters(characters);

			return game;
		}
		for (int i = 0; i < guess.length(); i++) {
			if (guess.charAt(i) == word.charAt(i)) {
				if (correctWord.get(guess.charAt(i)) != null && correctWord.get(guess.charAt(i)) > 0) {
					characters.put(i, Letter.CORRECT);
					correctWord.put(guess.charAt(i), correctWord.get(guess.charAt(i)) - 1);
				} else {
					characters.put(i, Letter.NOT_PRESENT);
				}
			}
		}
		for (int i = 0; i < guess.length(); i++) {

			if (word.contains(String.valueOf(guess.charAt(i)))) {
				if (correctWord.get(guess.charAt(i)) != null && correctWord.get(guess.charAt(i)) > 0) {
					characters.put(i, Letter.MISSPLACED);
					correctWord.put(guess.charAt(i), correctWord.get(guess.charAt(i)) - 1);
				} else if (characters.get(i) == null) {
					characters.put(i, Letter.NOT_PRESENT);
				}
			} else {
				characters.put(i, Letter.NOT_PRESENT);
			}

		}
		game.setGameStatus(GameStatus.IN_PROGRESS);

		if (numberOfTries + 1 > 6) {
			game.setGameStatus(GameStatus.LOSE);
			game.setCharacters(characters);
			game.setNumberOfTries(6);
			return game;
		}
		game.setCharacters(characters);
		game.setNumberOfTries(numberOfTries++);
		System.out.println("------------------------------------");
		correctWord.forEach((k, v) -> {
			System.out.println("Key: " + k + ", Value: " + v);
		});
		System.out.println("------------------------------------");
		characters.forEach((k, v) -> {
			System.out.println("Key: " + k + ", Value: " + v);
		});
		return game;
	}

}
