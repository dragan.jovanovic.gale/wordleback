package com.ftninformatika.jwd.wordle.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.wordle.model.Word;
import com.ftninformatika.jwd.wordle.repository.WordRepository;
import com.ftninformatika.jwd.wordle.service.LoadFileService;

@Service
public class JpaLoadFileService implements LoadFileService {

	@Autowired
	private WordRepository repo;

	@Override
	public void save(List<String> words) {

		for (String word : words) {
			Word word1 = new Word();
			word1.setWord(word);
			repo.save(word1);
		}
	}

}
