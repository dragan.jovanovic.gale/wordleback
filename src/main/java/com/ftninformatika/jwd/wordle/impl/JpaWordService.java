package com.ftninformatika.jwd.wordle.impl;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.wordle.model.Word;
import com.ftninformatika.jwd.wordle.repository.WordRepository;
import com.ftninformatika.jwd.wordle.service.WordService;

@Service
public class JpaWordService implements WordService {

	@Autowired
	private WordRepository wordRepo;

	@Override
	public Word getOne() {
		Long id = (long) getRandomNumberUsingNextInt(1, 2315);
		return wordRepo.findOneById(id);
	}

	public static int getRandomNumberUsingNextInt(int min, int max) {
		Random random = new Random();
		return random.nextInt(max - min + 1) + min;
	}

	@Override
	public Word findOneById(Long id) {
		return wordRepo.findOneById(id);
	}

	public Word findByWordIgnoreCaseContains(String word) {
		return wordRepo.findByWordIgnoreCaseContains(word);
	}

}
