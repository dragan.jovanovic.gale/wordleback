package com.ftninformatika.jwd.wordle.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.wordle.dto.WordDTO;
import com.ftninformatika.jwd.wordle.model.Word;

@Component
public class WordToDto implements Converter<Word, WordDTO> {

	@Override
	public WordDTO convert(Word source) {
		WordDTO dto = new WordDTO();
		dto.setId(source.getId());
		dto.setWord(source.getWord());
		return dto;
	}

	public List<WordDTO> convert(List<Word> list) {
		List<WordDTO> dto = new ArrayList<>();
		for (Word words : list) {
			dto.add(convert(words));
		}
		return dto;

	}

}
