package com.ftninformatika.jwd.wordle.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.wordle.dto.GameDto;
import com.ftninformatika.jwd.wordle.model.Game;
import com.ftninformatika.jwd.wordle.service.GameService;

@Component
public class DtoToGame implements Converter<GameDto, Game> {

	@Autowired
	private GameService gameService;

	@Override
	public Game convert(GameDto dto) {
		Game entity;
		entity = gameService.findOneById(dto.getId());
		System.out.println(entity.getNumberOfTries());
		entity.setNumberOfTries(entity.getNumberOfTries() + 1);
		System.out.println(entity.getNumberOfTries());

		entity.setGuessWord(dto.getGuessWord());

		return entity;
	}

}
