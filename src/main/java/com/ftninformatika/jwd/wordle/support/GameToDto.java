package com.ftninformatika.jwd.wordle.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.wordle.dto.GameDto;
import com.ftninformatika.jwd.wordle.model.Game;

@Component
public class GameToDto implements Converter<Game, GameDto> {

	@Override
	public GameDto convert(Game source) {
		GameDto dto = new GameDto();
		dto.setId(source.getId());
		dto.setNumberOfTries(source.getNumberOfTries());
		dto.setWordId(source.getWord().getId());
		dto.setWord(source.getWord().getWord());
		return dto;
	}

	public List<GameDto> convert(List<Game> list) {
		List<GameDto> dto = new ArrayList<>();
		for (Game games : list) {
			dto.add(convert(games));
		}
		return dto;

	}

}
