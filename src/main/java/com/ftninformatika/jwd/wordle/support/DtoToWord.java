package com.ftninformatika.jwd.wordle.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.wordle.dto.WordDTO;
import com.ftninformatika.jwd.wordle.model.Word;
import com.ftninformatika.jwd.wordle.service.WordService;

@Component
public class DtoToWord implements Converter<WordDTO, Word> {

	@Autowired
	private WordService service;

	@Override
	public Word convert(WordDTO dto) {
		Word entity;
		entity = service.findByWordIgnoreCaseContains(dto.getWord());

		return entity;
	}

}
