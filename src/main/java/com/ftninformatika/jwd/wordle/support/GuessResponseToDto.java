package com.ftninformatika.jwd.wordle.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.wordle.dto.GuessResponseDto;
import com.ftninformatika.jwd.wordle.model.GuessResponse;

@Component
public class GuessResponseToDto implements Converter<GuessResponse, GuessResponseDto> {

	@Override
	public GuessResponseDto convert(GuessResponse source) {
		GuessResponseDto dto = new GuessResponseDto();
		dto.setWord(source.getWord());
		dto.setCurrentTry(source.getCurrentTry());
		dto.setStatus(source.getStatus());
		dto.setCharacters(source.getCharacters());
		return dto;
	}

	public List<GuessResponseDto> convert(List<GuessResponse> list) {
		List<GuessResponseDto> dto = new ArrayList<>();
		for (GuessResponse guessResponse : list) {
			dto.add(convert(guessResponse));
		}
		return dto;

	}

}
