package com.ftninformatika.jwd.wordle.support;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.wordle.dto.GuessResponseSubmitDto;
import com.ftninformatika.jwd.wordle.model.GuessResponse;

@Component
public class DtoToGuessResponseSubmit implements Converter<GuessResponseSubmitDto, GuessResponse> {

	@Override
	public GuessResponse convert(GuessResponseSubmitDto source) {
		GuessResponse guessResponse = new GuessResponse();
		guessResponse.setGameId(source.getId());
		guessResponse.setWord(source.getWord());
		return guessResponse;
	}

}
