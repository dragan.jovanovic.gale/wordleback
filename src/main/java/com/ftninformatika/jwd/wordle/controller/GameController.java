package com.ftninformatika.jwd.wordle.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.wordle.dto.GameDto;
import com.ftninformatika.jwd.wordle.dto.GuessResponseDto;
import com.ftninformatika.jwd.wordle.dto.GuessResponseSubmitDto;
import com.ftninformatika.jwd.wordle.model.Game;
import com.ftninformatika.jwd.wordle.model.GuessResponse;
import com.ftninformatika.jwd.wordle.model.Word;
import com.ftninformatika.jwd.wordle.service.GameService;
import com.ftninformatika.jwd.wordle.service.WordService;
import com.ftninformatika.jwd.wordle.support.GameToDto;
import com.ftninformatika.jwd.wordle.support.GuessResponseToDto;

@RestController
@RequestMapping(value = "/api/game", produces = MediaType.APPLICATION_JSON_VALUE)
public class GameController {

	@Autowired
	private WordService wordService;

	@Autowired
	private GameService gameService;

	@Autowired
	private GameToDto toDto;

	@Autowired
	private GuessResponseToDto toGuess;

	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping()
	public ResponseEntity<GameDto> getOne() {
		Word word = wordService.getOne();
		Game game = new Game();
		game.setWord(word);
		game.setNumberOfTries(0);
		gameService.save(game);
		if (word != null) {
			return new ResponseEntity<>(toDto.convert(game), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<GameDto> getOne(@PathVariable Long id) {
		Game game = gameService.findOneById(id);

		if (game != null) {
			return new ResponseEntity<>(toDto.convert(game), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GuessResponseDto> create(@Valid @RequestBody GuessResponseSubmitDto guessResponseSubmitDto) {

		if (gameService.findOneById(guessResponseSubmitDto.getId()) == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		Game game1 = gameService.findOneById(guessResponseSubmitDto.getId());
		GuessResponse guessResponse = new GuessResponse();

		if (game1.getNumberOfTries() + 1 > 6) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		if (wordService.findByWordIgnoreCaseContains(guessResponseSubmitDto.getWord()) == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {

			game1.setNumberOfTries(game1.getNumberOfTries() + 1);

			guessResponse.setWord(game1.getWord().getWord());

			System.out.println();
			Game transferGame = gameService.checkLettersAndGameStatus(guessResponseSubmitDto.getWord(),
					game1.getWord().getWord(), game1.getNumberOfTries());

			System.out.println(transferGame.getCharacters());

			guessResponse.setCharacters(transferGame.getCharacters());
			guessResponse.setStatus(transferGame.getGameStatus());
			guessResponse.setCurrentTry(transferGame.getNumberOfTries());
			game1.setGameStatus(transferGame.getGameStatus());
			game1.setNumberOfTries(transferGame.getNumberOfTries());
			gameService.save(game1);
			return new ResponseEntity<>(toGuess.convert(guessResponse), HttpStatus.ACCEPTED);
		}
	}

}
