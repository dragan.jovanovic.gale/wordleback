package com.ftninformatika.jwd.wordle.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.wordle.dto.WordDTO;
import com.ftninformatika.jwd.wordle.model.Word;
import com.ftninformatika.jwd.wordle.service.LoadFileService;
import com.ftninformatika.jwd.wordle.service.WordService;
import com.ftninformatika.jwd.wordle.support.DtoToWord;
import com.ftninformatika.jwd.wordle.support.WordToDto;

@RestController
@RequestMapping(value = "/api/word", produces = MediaType.APPLICATION_JSON_VALUE)
public class WordController {

	@Autowired
	private WordService service;

	@Autowired
	private WordToDto toDto;

	@Autowired
	private LoadFileService serv;

	@Autowired
	private DtoToWord toEntity;

	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/loadWords")
	public ResponseEntity<WordDTO> loadWords() {
		File file = new File("C:\\Users\\hallo\\Desktop\\Wordle\\WordleBack\\wordle.txt");
		List<String> words = new ArrayList<>();
		try {
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				String st;
				while ((st = br.readLine()) != null)

					words.add(st);
			}
		} catch (FileNotFoundException e) {
			System.out.println("Greska pri ucitavanju");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Greska pri ucitavanju");
			e.printStackTrace();
		}
		serv.save(words);
		Word word = service.getOne();

		if (word != null) {
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/getWord")
	public ResponseEntity<WordDTO> getOne() {
		Word word = service.getOne();

		if (word != null) {
			return new ResponseEntity<>(toDto.convert(word), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/getWord/{id}")
	public ResponseEntity<WordDTO> getOne(@PathVariable Long id) {
		Word prijava = service.findOneById(id);

		if (prijava != null) {
			return new ResponseEntity<>(toDto.convert(prijava), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<WordDTO> create(@Valid @RequestBody WordDTO wordDTO) {
		Word word = toEntity.convert(wordDTO);

		System.out.println(word.getWord());
		Word wordToGuess = service.getOne();
		System.out.println(wordToGuess.getWord());
		return new ResponseEntity<>(toDto.convert(word), HttpStatus.ACCEPTED);
	}
}
