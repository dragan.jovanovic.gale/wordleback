package com.ftninformatika.jwd.wordle.enumer;

public enum Letter {
	CORRECT,
	MISSPLACED,
	NOT_PRESENT
}
