package com.ftninformatika.jwd.wordle.enumer;

public enum GameStatus {
	WIN,
	LOSE,
	IN_PROGRESS

}
