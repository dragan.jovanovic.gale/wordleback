package com.ftninformatika.jwd.wordle.model;

import java.util.LinkedHashMap;
import java.util.Map;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.ftninformatika.jwd.wordle.enumer.GameStatus;
import com.ftninformatika.jwd.wordle.enumer.Letter;

@Entity
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private int numberOfTries;

	@OneToOne
	private Word word;

	private String guessWord;

	private GameStatus gameStatus;

	@ElementCollection
	private Map<Integer, Letter> characters = new LinkedHashMap<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumberOfTries() {
		return numberOfTries;
	}

	public void setNumberOfTries(int numberOfTries) {
		this.numberOfTries = numberOfTries;
	}

	public Word getWord() {
		return word;
	}

	public void setWord(Word word) {
		this.word = word;
	}

	public String getGuessWord() {
		return guessWord;
	}

	public void setGuessWord(String guessWord) {
		this.guessWord = guessWord;
	}

	public GameStatus getGameStatus() {
		return gameStatus;
	}

	public void setGameStatus(GameStatus gameStatus) {
		this.gameStatus = gameStatus;
	}

	public Map<Integer, Letter> getCharacters() {
		return characters;
	}

	public void setCharacters(Map<Integer, Letter> characters) {
		this.characters = characters;
	}

}
