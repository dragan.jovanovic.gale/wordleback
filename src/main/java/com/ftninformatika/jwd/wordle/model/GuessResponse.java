package com.ftninformatika.jwd.wordle.model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.ftninformatika.jwd.wordle.enumer.GameStatus;
import com.ftninformatika.jwd.wordle.enumer.Letter;

@Entity
public class GuessResponse {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long gameId;

	private String word;

	private GameStatus status;

	@ElementCollection
	private Map<Integer, Letter> characters = new LinkedHashMap<>();

	private int currentTry;

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public GameStatus getStatus() {
		return status;
	}

	public void setStatus(GameStatus status) {
		this.status = status;
	}

	public Map<Integer, Letter> getCharacters() {
		return characters;
	}

	public void setCharacters(Map<Integer, Letter> characters) {
		this.characters = characters;
	}

	public int getCurrentTry() {
		return currentTry;
	}

	public void setCurrentTry(int currentTry) {
		this.currentTry = currentTry;
	}

	public GuessResponse() {
		super();
	}

	public Long getGameId() {
		return gameId;
	}

	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	public GuessResponse(String word, GameStatus status, Map<Integer, Letter> characters, List<String> guesses,
			int currentTry) {
		super();
		this.word = word;
		this.status = status;
		this.characters = characters;
		this.currentTry = currentTry;
	}

}
